// SPDX-License-Identifier: MIT
// pragma solidity >= 0.4.22 < 0.9.0;
pragma solidity ^0.8.0;

import "./ERC721Tradable.sol";

/**
* @title Creature
* Creature - a contract for my non-fungible creatures.
*/
contract Creature is ERC721Tradable {
    constructor(address _proxyRegistryAddress) ERC721Tradable("GCIT_TOKEN", "GCIT",
    _proxyRegistryAddress){}

    function baseTokenURI() override public pure returns (string memory) {
    return "https://api.npoint.io/0967204ad33698548696";
    }

    function contractURI() public pure returns (string memory) {
    return "https://api.npoint.io/7be6bc1bb2c6b5ee1d39";
    }

    function tokenURI(uint256 _tokenId) override public pure returns (string memory) {
    return string(abi.encodePacked(baseTokenURI(), Strings.toString(_tokenId)));
    // return baseTokenURI();
    }
}