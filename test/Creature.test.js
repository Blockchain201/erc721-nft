const Creature = artifacts.require("Creature");
const { expectRevert, BN, ether } = require("@openzeppelin/test-helpers");

contract("Creature", (accounts) => {
    let creatureInstance;
    const owner = accounts[0];
    const recipient = accounts[2];
    // console.log(owner);
    // console.log(recipient);
    beforeEach(async () => {
    creatureInstance = await Creature.new(owner, { from: owner });
    });

    it("should have the correct baseTokenURI", async () => {
        const expectedBaseTokenURI = "https://api.npoint.io/0967204ad33698548696";
        const baseTokenURI = await creatureInstance.baseTokenURI();
        assert.strictEqual(baseTokenURI,expectedBaseTokenURI,"Incorrect baseTokenURI");
    });

    it("should have the correct contractURI", async () => {
        const expectedContractURI = "https://api.npoint.io/7be6bc1bb2c6b5ee1d39";
        const contractURI = await creatureInstance.contractURI();
        assert.strictEqual(contractURI,expectedContractURI,"Incorrect contractURI");
    });

    it("should mint a token to the specified recipient", async () => {
        const tokenIdToMint = 1; // Specify the token ID to mint
        await creatureInstance.mintTo(recipient, { from: owner });
        const totalSupply = await creatureInstance.totalSupply();
        console.log("Total Supply:", totalSupply.toNumber());
        console.log("Token ID to Mint:", tokenIdToMint);
        const tokenOwner = await creatureInstance.ownerOf(tokenIdToMint);
        // console.log("Token Owner:", tokenOwner);
        assert.strictEqual(tokenOwner, recipient, "NFT should belong to recipient");
    });

    it("should not allow minting by a non-owner", async () => {
        const nonOwner = accounts[2];
        await expectRevert(creatureInstance.mintTo(recipient, { from: nonOwner }),"Ownable: caller is not the owner");
    });
});
